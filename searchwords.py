"""
Busca una palabra en una lista de palabras
"""

import sys
import sortwords


def search_words(word, words_list):
    try:
        test_index = sortwords.binary_search(words_list)
        return test_index
    except Exception:
        raise Exception("La palabra no se encontro en la lista.")


def main():
    if len(sys.argv) < 3:
        print("")
        sys.exit(1)

    target_word = sys.argv[1]
    word_list = sys.argv[2:]

    try:
        ordered_list = sortwords.sort(word_list)
        position = search_word(target_word, ordered_list)
        sortwords.show(ordered_list)
        print(position)

    except Exception as e:
        print(f"Error: {e}")
        sys.exit(1)


if __name__ == '__main__':
    main()
